package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Car;
import com.example.demo.Model.CarType;
import com.example.demo.Respository.CarRespository;
import com.example.demo.Respository.CarTypeRespository;


@RestController
@CrossOrigin
public class CarController {
    
    @Autowired
    CarRespository carRes;
	
	@Autowired
    CarTypeRespository carTypeRes;
	
	@GetMapping("/cars")
	public ResponseEntity<List<Car>> getCars() {
        try {
            List<Car> pCars = new ArrayList<Car>();

            carRes.findAll().forEach(pCars::add);

            return new ResponseEntity<>(pCars, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@GetMapping("/carTypes")
	public ResponseEntity<Set<CarType>> getCarTypeByCarCode(@RequestParam(value = "carCode") String carCode) {
        try {
                                                // findBy là hàm có sẵn , CountryCode là biến tạo bên Model Country chỉ cần viết hoa chũ cái đầu 
            Car vCar = carRes.findByCarCode(carCode);
            
            if(vCar != null) {
            	return new ResponseEntity<>(vCar.getCarType(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
